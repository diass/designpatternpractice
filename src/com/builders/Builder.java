package com.builders;

import com.components.Cooler;
import com.components.HardDrive;
import com.components.Processor;
import com.components.RAM;

public interface Builder {
    void setCooler(Cooler cooler);
    void setHardDrive(HardDrive hardDrive);
    void setProcessor(Processor processor);
    void setRam(RAM ram);
}
