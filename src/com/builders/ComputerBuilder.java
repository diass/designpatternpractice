package com.builders;

import com.components.Cooler;
import com.components.HardDrive;
import com.components.Processor;
import com.components.RAM;
import com.computers.Computer;

public class ComputerBuilder implements Builder {
    private Cooler cooler;
    private HardDrive hardDrive;
    private Processor processor;
    private RAM ram;

    @Override
    public void setCooler(Cooler cooler) {
        this.cooler = cooler;
    }

    @Override
    public void setHardDrive(HardDrive hardDrive) {
        this.hardDrive = hardDrive;
    }

    @Override
    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    @Override
    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public Computer getComputer() {
        return new Computer(cooler, hardDrive, processor, ram);
    }
}
