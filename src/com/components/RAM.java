package com.components;

public class RAM {
    private String brand;
    private String series;
    private int capacity;
    private int speed;
    private int timing;

    public RAM(String brand, String series, int capacity, int speed, int timing) {
        this.brand = brand;
        this.series = series;
        this.capacity = capacity;
        this.speed = speed;
        this.timing = timing;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getTiming() {
        return timing;
    }

    public void setTiming(int timing) {
        this.timing = timing;
    }
}
