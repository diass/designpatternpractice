package com.components;

public class Processor {
    private String brand;
    private String series;
    private String coreName;
    private int cores;
    private int threads;

    public Processor(String brand, String series, String coreName, int cores, int threads) {
        this.brand = brand;
        this.series = series;
        this.coreName = coreName;
        this.cores = cores;
        this.threads = threads;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getCoreName() {
        return coreName;
    }

    public void setCoreName(String coreName) {
        this.coreName = coreName;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }
}
