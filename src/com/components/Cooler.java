package com.components;

public class Cooler {
    private String brand;
    private String series;
    private double fanSize;
    private int RPM;
    private int noiseLevel;

    public Cooler(String brand, String series, double fanSize, int RPM, int noiseLevel) {
        this.brand = brand;
        this.series = series;
        this.fanSize = fanSize;
        this.RPM = RPM;
        this.noiseLevel = noiseLevel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public double getFanSize() {
        return fanSize;
    }

    public void setFanSize(double fanSize) {
        this.fanSize = fanSize;
    }

    public int getRPM() {
        return RPM;
    }

    public void setRPM(int RPM) {
        this.RPM = RPM;
    }

    public int getNoiseLevel() {
        return noiseLevel;
    }

    public void setNoiseLevel(int noiseLevel) {
        this.noiseLevel = noiseLevel;
    }
}
