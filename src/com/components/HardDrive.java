package com.components;

public class HardDrive {
    private String brand;
    private int capacity;
    private int RPM;
    private int cache;

    public HardDrive(String brand, int capacity, int RPM, int cache) {
        this.brand = brand;
        this.capacity = capacity;
        this.RPM = RPM;
        this.cache = cache;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRPM() {
        return RPM;
    }

    public void setRPM(int RPM) {
        this.RPM = RPM;
    }

    public int getCache() {
        return cache;
    }

    public void setCache(int cache) {
        this.cache = cache;
    }
}
