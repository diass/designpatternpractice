package com.computers;

import com.components.Cooler;
import com.components.HardDrive;
import com.components.Processor;
import com.components.RAM;

public class Computer {
    private final Cooler cooler;
    private final HardDrive hardDrive;
    private final Processor processor;
    private final RAM ram;

    public Computer(Cooler cooler, HardDrive hardDrive, Processor processor, RAM ram) {
        this.cooler = cooler;
        this.hardDrive = hardDrive;
        this.processor = processor;
        this.ram = ram;
    }

    public Cooler getCooler() {
        return cooler;
    }

    public HardDrive getHardDrive() {
        return hardDrive;
    }

    public Processor getProcessor() {
        return processor;
    }

    public RAM getRam() {
        return ram;
    }
}
